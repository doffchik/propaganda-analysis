import os

import pandas as pd
import nltk
nltk.download('stopwords')
from nltk.corpus import stopwords

import base_data
from config.settings import logger, DATASETS_DIR


class PreProcessingText:

    def __init__(self, text:str):
        self.text = text

        self._preprocessing_text()

    def __repr__(self):
        return self.__class__.__name__

    def _preprocessing_text(self):
        self.text = self.splitting_text(self.text)
        self.text = self.remove_numbers(self.text)
        self.text = self.remove_punctuation(self.text)

    def tokenize_text(self):
        pass

    @staticmethod
    def splitting_text(text):
        """
        this formatting method removes extra spaces and removes '\n' characters.
        As a result, writes the text in one string.
        """
        words = [
            word.lower()
            for line in text.split('\n')
            for word in line.split(' ')
        ]
        return ' '.join(words)

    @staticmethod
    def remove_punctuation(text):
        return ''.join([ch if ch not in base_data.punctuation else '' for ch in text])

    @staticmethod
    def remove_numbers(text):
        return ''.join([ch if not ch.isdigit() else '' for ch in text])



file_path = os.path.join(DATASETS_DIR, 'nltk_news_pravda', '1.csv')
df = pd.read_csv(file_path)
text = df.iloc[0]['text']


if __name__ == '__main__':

    pre = PreProcessingText(text=text)
    logger.debug(pre.text)
    res = stopwords.fileids()
    print(res)