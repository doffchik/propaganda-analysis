from nltk import FreqDist
from nltk.tokenize import word_tokenize

from config.settings import logger
from analysis import TextProcessor

def splitting_text(text: str) -> str:
    words = []

    for line in text.split('\n'):

        words.append(line)

    return ' '.join(words)

if __name__ == '__main__':
    proc = TextProcessor('nltk_news_pravda')
    file = proc.dataset_files_list[0]
    logger.debug(file)
    text = open(file).read()

    words = splitting_text(text)

    print(words)

    freq = FreqDist(words.split(' '))

    freq.plot()



