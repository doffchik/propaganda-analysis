import os
import time

import pandas as pd
import numpy as np

from tqdm import tqdm, trange
import nltk
from newspaper import Article

from config.settings import logger
from dataset_searching.news_parser import ParserNewsPravda
from config.settings import DATASETS_DIR


TQDM_PARAMETERS = dict(desc='Parsing-urls', unit='url')

class NLTKSiteParser(ParserNewsPravda):
    nltk.download('punkt')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.get_urls()

    def parse(self):
        for counter, url in enumerate(tqdm(self.urls_for_parsing, **TQDM_PARAMETERS)):
            article = Article(url)
            article.download()
            article.parse()
            article.nlp()

            file_path = os.path.join(self.path, f'{counter+1}.csv')
            self.store_article_data(article, url, file_path=file_path)

    @staticmethod
    def store_article_data(article: Article, url:str, file_path):
        data = {'title': article.title, '\turl': url, 'text': article.text,
                'topic': '', 'is_propaganda': ''}
        df = pd.DataFrame(data, index=[1])
        df.to_csv(file_path, index=False)


if __name__ == '__main__':
    url = 'https://www.pravda.com.ua/news/'

    path = os.path.join(DATASETS_DIR, 'nltk_news_pravda')
    NLTKSiteParser(url, path)()

