import string

allowed_punctuation = ["'", '"']
punctuation = ''.join([ch for ch in string.punctuation if ch not in allowed_punctuation])

KEYWORDS = ['бойовики', 'чеченські терористи', 'хохли', 'бульбаші чурки ']


generalization_WORDS = {
    'вони такі': 'погані'
} 