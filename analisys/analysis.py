import os
from pathlib import Path
from nltk.tokenize import word_tokenize

from config.settings import DATASETS_DIR


class PropagandaAnalyzer:

    def __init__(self, text):
        self.text = text

    def __repr__(self):
        return self.__class__.__name__

    def get_special_words(self):
        tokenized_text = word_tokenize(text=self.text)

        return tokenized_text


class TextProcessor:

    def __init__(self, dataset_folder: str):
        self.dataset_folder = self._validate_dataset_folder(dataset_folder)

    def _validate_dataset_folder(self, dataset_folder):
        path = os.path.join(DATASETS_DIR, dataset_folder)
        if not os.path.exists(path):
            AttributeError(f'This dataset folder is not exist. Check please input parameter: current - {dataset_folder}')
        return path

    @property
    def dataset_files_list(self):
        return tuple(Path(self.dataset_folder).glob('*'))

    def process_text(self):
        for file in list(self.dataset_files_list)[:1]:
            analyzer = PropagandaAnalyzer(open(file).read())
            res = analyzer.get_special_words()

            print(res)


if __name__ == '__main__':

    processor = TextProcessor('nltk_news_pravda')
    processor.process_text()
