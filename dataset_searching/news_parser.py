import os
from abc import ABC, abstractmethod

import requests
from urllib.parse import urlparse, urlunsplit
from bs4 import BeautifulSoup

from config.settings import logger
from config.settings import URLS_FOR_PARSING_DATASETS as URLS, DATASETS_DIR


class AbstractSiteParser(ABC):

    @abstractmethod
    def parse(self):
        pass

    @abstractmethod
    def get_urls(self):
        pass


class BaseSiteParser(AbstractSiteParser):

    BLOCK_WITH_ARTICLES_LIST = None
    HTML_TAG_NAME = None
    HTML_TAG_VALUE = None

    def __init__(self, url, path):
        self.url = url
        self.path = path
        self.response = None
        self.protocol = None
        self.hostname = None

        self.soup = None
        self.urls_for_parsing = None

        self._validate_main_attrs()

        self._pre_processing_url()

    def __call__(self, *args, **kwargs):
        self.parse()

    def __repr__(self):
        return f'<Parser: {self.__class__.__name__} | (site={self.hostname}), (response={self.response})>'

    def _pre_processing_url(self):
        self.response, response_text = self.get_url_response_and_text(url=self.url)

        if self.response.status_code == 200:
            logger.info(f'Response of url - {self.url}, is good')
            self.soup = self.get_soup(text=response_text)
            self.protocol, self.hostname = self.get_site_hostname_and_http_protocol(self.url)
        else:
            logger.error(f'Error in getting response from url. Status code: {self.response.status_code}')
            raise ProcessLookupError('Stop processing url')

    def _convert_hrefs_to_urls(self):
        self.urls_for_parsing = [
            urlunsplit((self.protocol, self.hostname, link, '', ''))
            for link in self.urls_for_parsing if not link.startswith(self.protocol)
        ]

    def _validate_main_attrs(self):
        if not (self.BLOCK_WITH_ARTICLES_LIST and self.HTML_TAG_VALUE):
            raise AttributeError('Parameters BLOCK_WITH_ARTICLES_LIST and HTML_TAG_VALUE cannot be None')

    def parse(self):
        self.get_urls()

        logger.info(f'{len(self.urls_for_parsing)} - articles are started parsing')
        for _url_iter, url in enumerate(self.urls_for_parsing):
            text = self.get_text_from_url_and_from_block(url, self.HTML_TAG_NAME, self.HTML_TAG_VALUE)

            if text:
                self.write_text_to_file(text, self.path, f'{_url_iter + 1}.txt')

    def get_urls(self):
        try:
            articles = self.soup.find_all('div', class_=self.BLOCK_WITH_ARTICLES_LIST)

            if len(articles) == 0:
                logger.critical(f'Parser cannot find the articles block with name "{self.BLOCK_WITH_ARTICLES_LIST}"')
                raise ProcessLookupError('Articles list is empty. Check self.PRAVDA_ARTICLES_BLOCK')

            else:
                logger.info(f'Count of founded articles: {len(articles)}')

            urls = [self.get_hrefs_from_div(article) for article in articles]
            self.urls_for_parsing = [link for link in urls if link]  # some urls in list can be None from function 'get_links_from_div'
            self._convert_hrefs_to_urls()
            logger.info('URL list successfully received')

        except BaseException as error:
            logger.error(error)

    @staticmethod
    def write_text_to_file(text, path, file_name):
        file_path = os.path.join(path, file_name)
        with open(file_path, 'w', encoding='utf-8') as file:
            file.write(text)
        logger.info(f'{file_name} - The text of the news article was successfully stored into the disk')

    @staticmethod
    def get_hrefs_from_div(div):
        tags_a = div.find_all('a')
        if len(tags_a) == 1:
            link = tags_a[0].get('href')
            return link
        else:
            logger.error('Some un expected variant of div tags "a" ')

    @classmethod
    def get_text_from_url_and_from_block(cls, url, tag_name, tag_value):
        _, text = cls.get_url_response_and_text(url)
        soup = cls.get_soup(text)
        div = soup.find_all(tag_name, class_=tag_value)
        if len(div) == 1:
            return div[0].text
        else:
            logger.warning(f'There are {len(div)} div with {cls.HTML_TAG_VALUE} class name block ')

    @staticmethod
    def get_url_response_and_text(url):
        response = requests.get(url=url)
        return response, response.text

    @staticmethod
    def get_soup(text: str) -> BeautifulSoup:
        soup = BeautifulSoup(text, 'html.parser')
        return soup

    @staticmethod
    def get_site_hostname_and_http_protocol(url):
        parsed_result = urlparse(url)
        return parsed_result.scheme, parsed_result.netloc


class ParserNewsPravda(BaseSiteParser):

    BLOCK_WITH_ARTICLES_LIST = 'article_news_list'
    HTML_TAG_NAME = 'div'
    HTML_TAG_VALUE = 'block_post'


class ParserNewsGazetaUa(BaseSiteParser):

    BLOCK_WITH_ARTICLES_LIST = 'clearfix'
    HTML_TAG_NAME = 'section'
    HTML_TAG_VALUE = 'article-content clearfix'

    def get_hrefs_from_div(self, div):
        tags_a = div.find_all('a')

        if len(tags_a) > 0:
            href = tags_a[0].get('href')

            if len(href.split(r'/')) != 1:
                return href


PARSERS = {
    'news_pravda': ParserNewsPravda,
    'news_gazeta_ua': ParserNewsGazetaUa,
}


class ParserController:

    def __init__(self, site_name, url):
        self.site_name = site_name
        self.url = url

        self._check_and_run_parser()

    def _check_and_run_parser(self):
        if PARSERS.get(self.site_name):
            path = self._create_dir_for_dataset()
            ParserClass = PARSERS.get(self.site_name)

            parser = ParserClass(self.url, path=path)
            parser()
        else:
            logger.error(f'Parser Controller dont support this web_site for parsing: {self.site_name}')

    def _create_dir_for_dataset(self):
        path = os.path.join(DATASETS_DIR, self.site_name)
        if not os.path.exists(path):
            os.makedirs(path)
        return path


if __name__ == '__main__':

    parser = ParserNewsGazetaUa(URLS['news_gazeta_ua'], DATASETS_DIR.join('news_gazeta_ua'))

