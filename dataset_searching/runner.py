from config.settings import logger
from config.settings import URLS_FOR_PARSING_DATASETS as URLS
from dataset_searching.news_parser import ParserController


def parser_runner():
    for site_name, url in URLS.items():
        logger.info(f'Start parsing {url}, url: {url}')

        ParserController(site_name, url)

