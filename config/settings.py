import os
from datetime import date
from loguru import logger

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

DATASETS_DIR = os.path.join(BASE_DIR, 'datasets')

LOGS_DIR = os.path.join(BASE_DIR, 'logs')

logger.add(
    os.path.join(LOGS_DIR, f'log({date.today().strftime("%d-%m-%y")}).log'),
    format='{time} {level} {module} {message}', compression='zip', rotation='00:00'
)

URLS_FOR_PARSING_DATASETS = {
    'news_pravda': 'https://www.pravda.com.ua/news/',
    'news_gazeta_ua': 'https://gazeta.ua/news',
}
