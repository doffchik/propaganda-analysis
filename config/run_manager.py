from config.settings import logger
from dataset_searching.runner import parser_runner


class Runner:

    def __init__(self, runner_name):
        self.runner_name = runner_name

        if self.runner_name == 'parser_runner':
            parser_runner()

        else:
            logger.error('This runner is not supported yet')
